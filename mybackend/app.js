const express = require('express')
var cors = require('cors')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')

const indexRouter = require('./routes/index')
const jobseekerRouter = require('./routes/jobseeker')
const LoginRouter = require('./routes/login')
const CompanyRouter = require('./routes/company')
const usersRouter = require('./routes/users')
const JobpostingRouter = require('./routes/jobposting')
const applicationRouter = require('./routes/application')
const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:27017/mydb',
  { useNewUrlParser: true, useUnifiedTopology: true })

const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('connect')
})

const app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

app.use('/', indexRouter)
app.use('/jobseeker', jobseekerRouter)
app.use('/login', LoginRouter)
app.use('/company', CompanyRouter)
app.use('/jobposting', JobpostingRouter)
app.use('/users', usersRouter)
app.use('/application', applicationRouter)

module.exports = app
